import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookComponent } from './booking-appointment/book/book.component';
import { ContactComponent } from './query/contact/contact.component';
import { ViewContactComponent } from './query/view-contact/view-contact.component';
import { ViewComponent } from './view-appointment/view/view.component';
import { LandingPageComponent } from './welcome-page/landing-page/landing-page.component';

const routes: Routes = [
  {path: 'landing-page', component: LandingPageComponent},
  {path: 'view-appointment', component: ViewComponent},
  {path: 'query', component: ContactComponent},
  {path: 'booking-appointment', component: BookComponent},
  {path: 'view-contact', component: ViewContactComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
