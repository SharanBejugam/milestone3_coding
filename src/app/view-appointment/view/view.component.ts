import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { HealthServiceService } from 'src/app/services/health-service.service';


@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  title: string = "View-Table";
  public data: any;
  constructor(private apiService : HealthServiceService) { }

  ngOnInit(): void {
    this.apiService.get().subscribe(response => {
    this.data = response;
  })
  }

}
