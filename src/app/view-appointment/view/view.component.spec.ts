import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';

import { ViewComponent } from './view.component';

describe('ViewComponent', () => {
  let component: ViewComponent;
  let fixture: ComponentFixture<ViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewComponent ],
      imports: [HttpClientModule],
      providers: [
        HttpClient,
        FormBuilder
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it(`firstname should be 'Sharan'`,() => {
  //   const fixture = TestBed.createComponent(ViewComponent);
  //   const view = fixture.componentInstance;
  //   expect(view.data[0].firstname).toEqual('Sharan');
  // })

  it(`title should be 'View-Table'`,() => {
    const fixture = TestBed.createComponent(ViewComponent);
    const view = fixture.componentInstance;
    expect(view.title).toEqual('View-Table');
  })
});
