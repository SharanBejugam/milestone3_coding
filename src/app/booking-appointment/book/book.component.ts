import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs';
import { HealthServiceService } from 'src/app/services/health-service.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit {

  constructor(private apiService: HealthServiceService, private formbuilder:FormBuilder) { }

  formSubmited=false;
  added=false;
  amount=500;

  myForm = this.formbuilder.group({
    firstname: ["", [Validators.required]],
    lastname: ["", [Validators.required]],
    age: ["", [Validators.required]],
    phonenumber: ["", [Validators.required]],
    email: ["", [Validators.required]],
    streetaddress: ["", [Validators.required]],
    city: ["", [Validators.required]],
    state: ["", [Validators.required]],
    country: ["", [Validators.required]],
    pincode: ["", [Validators.required]],
    trainerpreference: ["", [Validators.required]],
    physiotherapist: ["", [Validators.required]],
    packages: ["package1", [Validators.required]],
    weeks:["1", [Validators.required]],
    amount:[this.amount],
  });

  ngOnInit(): void {
    this.myForm.controls['amount'].disable();
  }

  bookappointment(){
    
    console.log(this.myForm)
    if(this.myForm.status == "VALID"){
      let body ={
      firstname: this.myForm.value.firstname,
      lastname: this.myForm.value.lastname,
      age: this.myForm.value.age,
      phonenumber: this.myForm.value.phonenumber,
      email: this.myForm.value.email,
      streetaddress: this.myForm.value.streetaddress,
      city: this.myForm.value.city,
      state: this.myForm.value.state,
      country: this.myForm.value.country,
      pincode: this.myForm.value.pincode,
      trainerpreference: this.myForm.value.trainerpreference,
      physiotherapist: this.myForm.value.physiotherapist,
      packages: this.myForm.value.packages,
      weeks: this.myForm.value.weeks,
      amount: this.amount
      };

      console.log(body)

      this.apiService.post(body).subscribe(res=>{
        console.log(res)
      });

      alert("Submitted successfully!")
    }
    else{
      alert('All fields are required.');
    }
  }

  add(){
    console.log("add caleed")
    this.amount = this.amount+200;
    this.added = true;
  }

  sub(){
    if(this.added)
    {
      this.amount=this.amount-200;
      this.added=false;
    }
  }

  w1: any = this.myForm.value.weeks;
  pack(){
    let p1 = this.myForm.value.physiotherapist == 'yes'?200:0;
    if(this.myForm.value.packages == 'package2')
    {
      this.amount = p1+ this.w1*4*400;
    }
    if(this.myForm.value.packages == 'package3')
    {
      this.amount = p1+ this.w1*5*300;
    }
    if(this.myForm.value.packages =='package1')
    {
      this.amount = p1+500;
    }
  }
}
