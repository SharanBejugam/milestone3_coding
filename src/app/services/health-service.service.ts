import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HealthServiceService {

  baseUrl : string = "http://localhost:3000/health-member";
  contactUrl: string = "http://localhost:3000/contact-us";

  constructor(private http: HttpClient) { }

  get(){
    return this.http.get(this.baseUrl);
  }

  post(appointment: any){
    return this.http.post(this.baseUrl,appointment);
  }

  getContact(){
    return this.http.get(this.contactUrl);
  }

  postContact(contact: any){
    return this.http.post(this.contactUrl,contact);
  }
}
