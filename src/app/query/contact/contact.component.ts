import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { HealthServiceService } from 'src/app/services/health-service.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  constructor(private apiService: HealthServiceService, private formbuilder: FormBuilder) { }

  myForm = this.formbuilder.group({
    firstname: ['', Validators.required],
    lastname: ['', Validators.required],
    email: ['', Validators.required],
    message: ['', Validators.required]
   })

  ngOnInit(): void {

  }

  addquery(){
    if(this.myForm.status == 'VALID'){
      let query = {
        firstname: this.myForm.value.firstname,
        lastname: this.myForm.value.lastname,
        email: this.myForm.value.email,
        message: this.myForm.value.message
      }

      this.apiService.postContact(query).subscribe(contact => {
        console.log(contact);
      })

      alert('Submitted successfully');
    }

    else{
      alert('All fields are required.');
    }
  }
}
