import { Component, OnInit } from '@angular/core';
import { HealthServiceService } from 'src/app/services/health-service.service';

@Component({
  selector: 'app-view-contact',
  templateUrl: './view-contact.component.html',
  styleUrls: ['./view-contact.component.scss']
})
export class ViewContactComponent implements OnInit {

  constructor(private apiService: HealthServiceService) { }

  data: any;

  ngOnInit(): void {
    this.apiService.getContact().subscribe(res => {
      this.data = res;
      console.log(this.data);
    })
  }

}
