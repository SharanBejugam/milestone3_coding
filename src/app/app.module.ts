import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BookingAppointmentModule } from './booking-appointment/booking-appointment.module';
import { QueryModule } from './query/query.module';
import { ViewAppointmentModule } from './view-appointment/view-appointment.module';
import { WelcomePageModule } from './welcome-page/welcome-page.module';
import { FooterComponent } from './common-components/footer/footer.component';
import { HealthInterceptor } from './health.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BookingAppointmentModule,
    ViewAppointmentModule,
    QueryModule,
    WelcomePageModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [
    {provide:HTTP_INTERCEPTORS,useClass:HealthInterceptor,multi:true},
    HttpClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
